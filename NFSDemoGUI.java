import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import jschutil.JSchUtil;


public class NFSDemoGUI {
    public static void main(String args[]){
       String user = "cis";
       String host = "134.88.13.237";
       int port = 22;
       String pass = "SeniorDesign";

       var abc = new JSchUtil(user, host, port, pass);
       
       String getPodsScript = 
       "kubectl get pods "+
       "--all-namespaces "+
       "-o custom-columns=NAME:.metadata.name,STATUS:.status.phase,NODE:.spec.nodeName | grep daemonset";
       
       //fixes the spacing of the pod output header
       StringBuilder builder = new StringBuilder();
       builder.append("NAME \t\t" +"                  " +"STATUS" + "     " + "NODE\n");
       String tmp = builder.toString();
          
       String sharedFileScript = "cat /mnt/nfs_share/test-daemon.txt | tail -n3";

       setUIFont (new javax.swing.plaf.FontUIResource("Mono",Font.PLAIN,32));
       
       JFrame frame = new JFrame("NFS Test Demo");
       frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       frame.setSize(800,600);
       frame.setLayout(new GridLayout(4, 1,  10, 10));
       
       JButton button = new JButton("Show the daemon pods that are running");
       
       JTextArea text1 = new JTextArea();
       JButton button2 = new JButton("Show the last three lines of the text file");
       JTextArea text2 = new JTextArea();
       frame.getContentPane().add(button); // Adds Button to content pane of frame
       frame.getContentPane().add(text1); // Adds Button to content pane of frame
       frame.getContentPane().add(button2); // Adds Button to content pane of frame
       frame.getContentPane().add(text2); // Adds Button to content pane of frame
       frame.setVisible(true);
       
       button.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
        	   text1.setText(tmp +abc.runScript(getPodsScript));
           }
       });
       
       button2.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
        	   text2.setText(abc.runScript(sharedFileScript));
           }
       });
    }
  private static void setUIFont (javax.swing.plaf.FontUIResource f){
    java.util.Enumeration keys = UIManager.getDefaults().keys();
    while (keys.hasMoreElements()) {
      Object key = keys.nextElement();
      Object value = UIManager.get (key);
      if (value instanceof javax.swing.plaf.FontUIResource)
        UIManager.put (key, f);
    }
  }
}
