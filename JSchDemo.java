import java.io.ByteArrayOutputStream;
import com.jcraft.jsch.*;

public class JSchDemo
{
    // connect, then run a single command
    public static void main(String[] args)
    {
        String user = "cis";
        String host = "134.88.13.237";
        int port = 22;
        String pass = "SeniorDesign";
        
        String[] commands = {
            // multi-command sequences must be in the same string
            "cd git\nls", 
            // compare results of above with results of 2 below cmds
            "cd git",
            "ls", 
            "echo Hello, World!"
        };

        // get "handle" to ssh connection
        Session session = startSession(user, host, pass, port);
        // use and disconnect the session when done
        if (session != null)
        {
            runCmdsThruSession(session, commands);
            session.disconnect();
        }

        return;
    }

    public static Session startSession(String user, String host, String pass, int port)
    {
        Session session = null;
        try
        { 
            session = new JSch().getSession(user, host, port);
            session.setPassword(pass);
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect(50);
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        return session;
    }

    public static void runCmdsThruSession(Session session, String[] commands)
    {
        ChannelExec channel = null;
        var responseStream = new ByteArrayOutputStream();
        String responseString;

        try
        {
            for (String s : commands)
            {
                // channel is mechanism we will send commands through
                channel = (ChannelExec) session.openChannel("exec");
                channel.setCommand(s);
                responseStream = new ByteArrayOutputStream();
                channel.setOutputStream(responseStream);
                // execute the command
                channel.connect();

                // wait for the command to finish executing
                while (channel.isConnected()) ;

                responseString = new String(responseStream.toByteArray());
                System.out.println("*** command: ***\n" + s + "\n*** output: ***\n" + responseString);
            }
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        finally
        {
            if (channel != null) channel.disconnect();
        }
    }
}
