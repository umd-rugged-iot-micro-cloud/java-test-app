import java.io.ByteArrayOutputStream;
import com.jcraft.jsch.*;

public class JSchUtil
{
  Session session;
  // connect, then run a single command
  public static void main(String[] args)
  {
    String user = "cis";
    String host = "134.88.13.237";
    int port = 22;
    String pass = "SeniorDesign";
    String script = "ls";
    
    var ssh = new JSchUtil(user, host, port, pass);
    // use and disconnect the session when done
    System.out.println(ssh.runScript(script));
    System.out.println(ssh.runScript(script));
    ssh.exit(); // necessary

    return;
  }

  public JSchUtil(String user, String host, int port, String pass)
  {
    session = startSession(user, host, pass, port);
  }
  public void exit()
  {
    if (session != null && session.isConnected())
      session.disconnect();
  }

  public Session startSession(String user, String host, String pass, int port)
  {
    try
    { 
      session = new JSch().getSession(user, host, port);
      session.setPassword(pass);
      session.setConfig("StrictHostKeyChecking", "no");
      session.connect(1000);
    }
    catch (Exception e)
    {
        System.out.println(e);
    }
    return session;
  }

  public String runScript(String script)
  {
    ChannelExec channel = null;
    String responseString = null;
    var responseStream = new ByteArrayOutputStream();

    try
    {
      // channel is mechanism we will send commands through
      channel = (ChannelExec) session.openChannel("exec");
      channel.setCommand(script);
      channel.setOutputStream(responseStream);
      // execute the command
      channel.connect();

      // wait for the command to finish executing
      while (channel.isConnected()) ;

      responseString = new String(responseStream.toByteArray());
    }
    catch (Exception e) 
    { 
      System.out.println(e); 
    }
    finally 
    { 
      if (channel != null) channel.disconnect(); 
    }

    return responseString;
  }
}
