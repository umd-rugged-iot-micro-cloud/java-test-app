import java.awt.*;
import java.io.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import jschutil.JSchUtil;
import java.io.File;  // Import the File class
import java.io.FileNotFoundException;  // Import this class to handle errors
import java.util.Scanner; // Import the Scanner class to read text files


public class CicdGui{

  JTextArea testPodOutput;
  JTextArea editorText;
  JSchUtil ssh;

  public static void main (String[] args)
  {
    String user = "cis";
    String host = "134.88.13.237";
    int port = 22;
    String pass = "SeniorDesign";
    
    var gui = new CicdGui(user,host,port,pass);
  }

  public CicdGui(String user, String host, int port, String pass) {

    // first set up the ssh session
    ssh = new JSchUtil(user, host, port, pass);

    setUIFont (new javax.swing.plaf.FontUIResource("Mono",Font.PLAIN,32));

    // this is the window we're using
    JFrame frame = new JFrame("CICD Demo App");
    frame.setLayout(new BorderLayout());

    /*******************************************/
    // left side of the screen
    JPanel panelLeft = new JPanel(new BorderLayout());
    panelLeft.setBorder(BorderFactory
        .createTitledBorder("App state monitoring"));

    // add a button and a text box
    var testPodButton = new JButton("Get current deployed app output");
    testPodOutput = new JTextArea();
    testPodOutput.setEditable(false);
    // make button interact with text box
    testPodButton.addActionListener( new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        updateTestPodOutput();
      }
    });
    // add above elements to frame
    panelLeft.add(testPodButton, BorderLayout.NORTH);
    panelLeft.add(testPodOutput, BorderLayout.CENTER);

    // add panel
    frame.add(panelLeft, BorderLayout.WEST);

    /*******************************************/
    // right side of the screen
    JPanel panelRight = new JPanel(new BorderLayout());
    panelRight.setBorder(
        BorderFactory.createTitledBorder("Pipeline controls"));

    // create some elements to put into the new panel
    var editor = new JPanel(new BorderLayout());
    editor.setBorder(BorderFactory.createTitledBorder("editor"));

    editorText = new JTextArea(); // will hold the text
    var editorTextScr = new JScrollPane(editorText); // makes text scrollable
                                                     
    var editorButtons = new JPanel(new BorderLayout());
    var editorLoadButton = new JButton("Load");
    var editorSaveButton = new JButton("Save");
    editorTextScr.setVerticalScrollBarPolicy(
        JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

    // add functionality to buttons
    editorLoadButton.addActionListener( new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        loadTestProgram();
      }
    });
    editorSaveButton.addActionListener( new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        writeTestProgram();
      }
    });

    // combine above elements to create the editor
    editorButtons.add(editorLoadButton, BorderLayout.NORTH);
    editorButtons.add(editorSaveButton, BorderLayout.SOUTH);
    editor.add(editorButtons, BorderLayout.EAST);
    editor.add(editorTextScr, BorderLayout.CENTER);
    // now add the editor to the right panel
    panelRight.add(editor, BorderLayout.CENTER);

    // make a new element to contain all the rest of the elements
    var pipelinePanel = new JPanel(new BorderLayout());
    pipelinePanel.setBorder(
        BorderFactory.createTitledBorder("CICD pipeline controls"));
    // new buttons
    var gitPushButton = new JButton("Push changes to source code repository");
    var argocdButton = new JButton("Force update Argo CD");
    var jenkinsReminder = new JTextArea("(External: force update Jenkins)");
    pipelinePanel.add(gitPushButton, BorderLayout.NORTH);
    pipelinePanel.add(jenkinsReminder, BorderLayout.CENTER);
    pipelinePanel.add(argocdButton, BorderLayout.SOUTH);
    jenkinsReminder.setEditable(false); // make it so the jenkins reminder can't be edited
                                        
    // make the buttons functional
    gitPushButton.addActionListener( new ActionListener() {
      @Override
      public void actionPerformed (ActionEvent e) {
        // this is analogous to what will happen when we wait for the command to execute
        // this is great because the button stays grey while the command is executing
        gitCommitPush();
        return;
      }
    });
    argocdButton.addActionListener( new ActionListener() {
      @Override
      public void actionPerformed (ActionEvent e) {
        System.out.println(ssh.runScript(
          "argocd login $ARGOCDIP --insecure --username=admin ==password=SeniorDesign; " +
          "argocd app sync hello-world"));

      	return;
      }
    });

    panelRight.add(pipelinePanel, BorderLayout.SOUTH);
    frame.add(panelRight);

    frame.setSize(500, 500);
    frame.setVisible(true);
  }
  
  // utility function for making the font bigger
  private static void setUIFont (javax.swing.plaf.FontUIResource f){
    java.util.Enumeration keys = UIManager.getDefaults().keys();
    while (keys.hasMoreElements()) {
      Object key = keys.nextElement();
      Object value = UIManager.get (key);
      if (value instanceof javax.swing.plaf.FontUIResource)
        UIManager.put (key, f);
    }
  }

  private void updateTestPodOutput()
  {
    String script = "kubectl logs -ntests hello-world";
    String output = ssh.runScript(script);
    testPodOutput.append(output);
  }   
  private void loadTestProgram()
  {
    try 
    {
      FileReader fr = new FileReader("../ci-staging-area/hello-world/src/hello_world.c");
      String str = "";
      int i;
      // action to replace test function with
      while ((i = fr.read()) != -1) {
        str += (char)i;
      }
      editorText.setText(str);
      fr.close();
    }
    catch (IOException i) 
    {
      // If there is no file in specified path or
      // any other error occurred during runtime
      // then it will print IOException

      // Display message
      System.out.println(
          "There are some IOException");
    }
  }
  private void writeTestProgram()
  {
    try 
    {
      FileWriter fr = new FileWriter("../ci-staging-area/hello-world/src/hello_world.c");
      fr.write(editorText.getText());
      fr.close();
    }
    catch (IOException i) 
    {
      System.out.println(
                  "There are some IOException");
    }
  }
  
  //To be implemented - function for executing shell commands
  public void gitCommitPush()
  {
    StringBuffer output = new StringBuffer();

    String[] cmds = {
      //"cd", "../ci-staging-area/;",
      //"ls;",
      "git","commit","-am","\"automated","demo","commit\";",
      "git","pull;",
      "git","push;",
    };

    var rt = Runtime.getRuntime();
    System.out.println(String.join(" ", cmds));
    try{
      var p = rt.exec(cmds, null, new File("../ci-staging-area/"));
     
      BufferedReader input = new BufferedReader(new InputStreamReader(
               p.getInputStream()));
      BufferedReader error = new BufferedReader(new InputStreamReader(
               p.getErrorStream()));

      String line = null;
      while ((line = input.readLine()) != null)
      {
        System.out.println(line);
      }
      line = null;
      while ((line = input.readLine()) != null)
      {
        System.out.println(line);
      }
      int exitVal = p.waitFor();
         System.out.println("Exited with error code " + exitVal);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
}
